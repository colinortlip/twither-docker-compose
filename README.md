# Twither Docker Compose

One Paragraph of project description goes here

## Getting Started

Download the folder

### Prerequisites

Will need docker installed

### How to run

To run with preconstructed data:
```
docker-compose up
```

To run without preconstructed data (empty tables):
To run with proconstructed data entries

```
docker-compose -f docker-compose-no-data.yml up
```