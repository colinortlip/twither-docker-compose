--
-- Name: twitter_user; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.twitter_user (
    id integer NOT NULL PRIMARY KEY,
    username character varying(288) NOT NULL,
    password character varying(288) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    first_name character varying(288),
    last_name character varying(288),
    email character varying(288) NOT NULL,
    bio character varying(500)
);
ALTER TABLE public.twitter_user OWNER TO postgres;


--
-- Name: twitter_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--
CREATE SEQUENCE IF NOT EXISTS public.twitter_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.twitter_user_id_seq OWNER TO postgres;


--
-- Name: twitter_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--
ALTER SEQUENCE public.twitter_user_id_seq OWNED BY public.twitter_user.id;


--
-- Name: twitter_user id; Type: DEFAULT; Schema: public; Owner: postgres
--
ALTER TABLE ONLY public.twitter_user ALTER COLUMN id SET DEFAULT nextval('public.twitter_user_id_seq'::regclass);


--
-- Name: tweet; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.tweet (
    id integer NOT NULL PRIMARY KEY,
    content character varying(500) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    user_id integer NOT NULL,
    parent_id integer
);
ALTER TABLE public.tweet OWNER TO postgres;


--
-- Name: tweet_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--
CREATE SEQUENCE IF NOT EXISTS public.tweet_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.tweet_id_seq OWNER TO postgres;


--
-- Name: tweet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--
ALTER SEQUENCE public.tweet_id_seq OWNED BY public.tweet.id;


--
-- Name: tweet id; Type: DEFAULT; Schema: public; Owner: postgres
--
ALTER TABLE ONLY public.tweet ALTER COLUMN id SET DEFAULT nextval('public.tweet_id_seq'::regclass);


--
-- Name: tweet tweet_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE tweet DROP CONSTRAINT IF EXISTS tweet_user_id_fkey;
ALTER TABLE ONLY public.tweet
    ADD CONSTRAINT tweet_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.twitter_user(id);



--
-- Name: user_follow; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.user_follow (
    follower_id integer NOT NULL,
    followed_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    PRIMARY KEY (follower_id, followed_id)
);
ALTER TABLE public.user_follow OWNER TO postgres;

--
-- Name: topic; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.topic (
    topic_name character varying(255) NOT NULL PRIMARY KEY
);
ALTER TABLE public.topic OWNER TO postgres;


--
-- Name: topic_follow; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.topic_follow (
    follower_id integer NOT NULL,
    followed_topic character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now(),
    PRIMARY KEY (follower_id, followed_topic)
);
ALTER TABLE public.topic_follow OWNER TO postgres;


--
-- Name: topic_follow followed_topic_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE topic_follow DROP CONSTRAINT IF EXISTS followed_topic_fkey;
ALTER TABLE ONLY public.topic_follow
    ADD CONSTRAINT followed_topic_fkey FOREIGN KEY (followed_topic) REFERENCES public.topic(topic_name);


--
-- Name: topic_follow follower_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE topic_follow DROP CONSTRAINT IF EXISTS follower_id_fkey;
ALTER TABLE ONLY public.topic_follow
    ADD CONSTRAINT follower_id_fkey FOREIGN KEY (follower_id) REFERENCES public.twitter_user(id);


--
-- Name: tweet_share; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.tweet_share (
    sharer_id integer NOT NULL,
    shared_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    PRIMARY KEY (sharer_id, shared_id)
);
ALTER TABLE public.tweet_share OWNER TO postgres;


--
-- Name: tweet_share shared_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE tweet_share DROP CONSTRAINT IF EXISTS shared_id_fkey;
ALTER TABLE ONLY public.tweet_share
    ADD CONSTRAINT shared_id_fkey FOREIGN KEY (shared_id) REFERENCES public.tweet(id);


--
-- Name: tweet_share sharer_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE tweet_share DROP CONSTRAINT IF EXISTS sharer_id_fkey;
ALTER TABLE ONLY public.tweet_share
    ADD CONSTRAINT sharer_id_fkey FOREIGN KEY (sharer_id) REFERENCES public.twitter_user(id);


--
-- Name: topic_mention; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.topic_mention (
    mentioner_id integer NOT NULL,
    mentioned_topic character varying(255) NOT NULL,
    created timestamp without time zone DEFAULT now(),
    PRIMARY KEY (mentioner_id, mentioned_topic)
);
ALTER TABLE public.topic_mention OWNER TO postgres;


--
-- Name: topic_mention mentioned_topic_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE topic_mention DROP CONSTRAINT IF EXISTS mentioned_topic_fkey;
ALTER TABLE ONLY public.topic_mention
    ADD CONSTRAINT mentioned_topic_fkey FOREIGN KEY (mentioned_topic) REFERENCES public.topic(topic_name);


--
-- Name: topic_mention mentioner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE topic_mention DROP CONSTRAINT IF EXISTS mentioner_id_fkey;
ALTER TABLE ONLY public.topic_mention
    ADD CONSTRAINT mentioner_id_fkey FOREIGN KEY (mentioner_id) REFERENCES public.tweet(id);


--
-- Name: user_mention; Type: TABLE; Schema: public; Owner: postgres
--
CREATE TABLE IF NOT EXISTS public.user_mention (
    mentioner_id integer NOT NULL,
    mentioned_id integer NOT NULL,
    created timestamp without time zone DEFAULT now(),
    PRIMARY KEY (mentioner_id, mentioned_id)
);
ALTER TABLE public.user_mention OWNER TO postgres;


--
-- Name: user_mention mentioned_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE user_mention DROP CONSTRAINT IF EXISTS mentioned_id_fkey;
ALTER TABLE ONLY public.user_mention
    ADD CONSTRAINT mentioned_id_fkey FOREIGN KEY (mentioned_id) REFERENCES public.twitter_user(id);


--
-- Name: user_mention mentioner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--
ALTER TABLE user_mention DROP CONSTRAINT IF EXISTS mentioner_id_fkey;
ALTER TABLE ONLY public.user_mention
    ADD CONSTRAINT mentioner_id_fkey FOREIGN KEY (mentioner_id) REFERENCES public.tweet(id);